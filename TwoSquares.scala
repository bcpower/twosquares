import math._

object TwoSquares extends App {
  val original = args(0).toInt
  val maxSquare = round(sqrt(original)).asInstanceOf[Int]

  getSquares(original,maxSquare,0)

  def getSquares(original:Int, max:Int, min:Int){
    val temp = square(max) + square(min)
    
    if(max >= min){
      if(temp < original){
        getSquares(original,max,min+1)
      }else if(temp > original){
        getSquares(original,max-1,min)
      }else{
        println(max+"^2 + "+min+"^2 = "+original)
        getSquares(original,max-1,min)
      }
    }
  }

  def square(x:Int): Int = {
    x * x
  }
}
